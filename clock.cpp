#include "Clock.h"

#include <iostream>
using namespace std;

void CClock::setTime(int Hours, int Minutes, int Seconds) 
{
	if (0 <= Hours && Hours < 24)
		hr = Hours;
	else
		hr = 0;

	if (0 <= Minutes && Minutes < 60)
		min = Minutes;
	else
		min = 0;

	if (0 <= Seconds && Seconds < 60)
		sec = Seconds;
	else
		sec = 0;
}
CClock::CClock(int Hours, int Minutes) 
{
	hr = Hours;
	min = Minutes;
}

void CClock::incrementHours() 
{
	hr++;
	if (hr > 23)
		hr = 0;
}

void CClock::incrementMinutes() 
{
	min++;
	if (min > 59) {
		min = 0;
		incrementHours(); //increment hours
	}
}

void CClock::incrementSeconds() 
{
	sec++;
	if (sec > 59) {
		sec = 0;
		incrementMinutes(); //increment minutes
	}
}

void CClock::incrementSeconds(int x) 
{
	sec += x;
	if (sec > 59) {
		sec = 0;
		incrementMinutes(); //increment minutes
	}
}
bool CClock::equalTime(const CClock& otherClock) const
{
	return (hr == otherClock.hr
		&& min == otherClock.min
		&& sec == otherClock.sec);

}
void CClock::compare(CClock c1, CClock c2)
{
	if (c1.hr == c2.hr && c1.min == c2.min && c1.sec == c2.sec) cout << "They are equal"<<endl;
	else cout << "They are not equal" << endl;
}
void CClock::printTime() const
{
	if (hr <10)
		cout << "0";
	cout << hr << ":";
	if (min <10)
		cout << "0";
	cout << min << ":";
	if (sec <10)
		cout << "0";
	cout << sec;
}
